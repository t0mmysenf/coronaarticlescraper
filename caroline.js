const newsapi = require('./mods/newsApi');
const db = require("./services/dbconnection");

var counter = 0;
db.connectAndTest();
setInterval(function(){
    counter += 1;
    console.log("Durchlauf Nr: " + counter);

    //newsapi.org
    newsapi.request("ch").then(result => {newsapi.parseArticles(result)});
    newsapi.request("us").then(result => {newsapi.parseArticles(result)});
    
}, 180 * 1000);