class Article {
    constructor (source, author, title, description, url, date, content){
        this.source = source;
        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.date = date;
        this.content = content;
    }
}

module.exports = Article;