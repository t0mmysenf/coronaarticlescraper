/*
    Script für DB
    Starten mit: mysql -h localhost < ./DB/tabchange_001.sql
*/

USE carolinedb;

ALTER TABLE article
ADD COLUMN botId INT AFTER linkhash;
