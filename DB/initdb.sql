/*
    Script für DB
    Starten mit: mysql -h localhost < ./DB/initdb.sql
*/

/*
DROP DATABASE IF EXISTS `carolinedb`;
*/

CREATE DATABASE IF NOT EXISTS carolinedb;

USE carolinedb;

CREATE USER IF NOT EXISTS 'caroline'@'localhost' IDENTIFIED BY "caroline";

/*
Falls auf Gitpod, die obere Zeile mit der Folgenen Line ersetzen (NICHT VERGESSEN, ES DANACH WIEDER ZU ÄNDERN):
CREATE USER IF NOT EXISTS 'caroline'@'localhost' IDENTIFIED WITH mysql_native_password BY "fpq349fgn3tpoiu4hlkfjpo3i";
*/
GRANT ALL PRIVILEGES ON carolinedb.* TO 'caroline'@'localhost';
FLUSH PRIVILEGES;

/*
DROP TABLE IF EXISTS `article`;
*/
CREATE TABLE IF NOT EXISTS article (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    source TEXT,
    author TEXT,
    title TEXT,
    subtitle TEXT,
    link TEXT,
    publicationdate DATETIME,
    content TEXT,
    linkhash VARCHAR(100),
    UNIQUE KEY unique_linkhash (linkhash)
);

