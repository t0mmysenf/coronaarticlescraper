const mysql = require("mysql");
const crypto = require("crypto");
const moment = require("moment");
//Conffile
var conf = require("../services/confservice");

var con = mysql.createConnection({
  host: "localhost",
  user: "caroline",
  password: "caroline",
  database: "carolinedb"
});

module.exports = {
    insertArticle: function(article) {
        var linkhash = require("crypto")
                        .createHash("sha256")
                        .update(article.url)
                        .digest("hex");
        
        


        var sql = "INSERT INTO article (source, author, title, subtitle, link, publicationdate, content, linkhash, botId) VALUES (?)";
        var values = [article.source, article.author, article.title, article.description, article.url, moment(article.date).format("YYYY-MM-DD hh:mm:ss"), article.content, linkhash, conf.getBotId()]
        
        con.query(sql, [values], function (err, result) {
            if(err) {
                console.log("Ein Fehler ist beim Speichern aufgetreten: ");

                if(err.code == "ER_DUP_ENTRY") {
                    console.log("Der artikel mit dem Hashlink " + linkhash + " ist schon vorhanden.");
                } else if(err.code == "ER_NOT_SUPPORTED_AUTH_MODE") {
                    throw err;
                } else {
                    console.log(err);
                }
            }
        });
    },

    connectAndTest: function(){
        con.connect(function(err) {
            if (err) throw err;

            console.log('Connected to the MySQL server.');
        });
    }
}
