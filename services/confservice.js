const fs = require('fs');
const path = require('path');
var confpath = path.resolve(__dirname, '../carolineconf/conf.json');
const conf = JSON.parse(fs.readFileSync(confpath, 'utf8'));


module.exports = {
    getSearchKeywords: function(){
        return conf.searchterms;
    },
    getBotId: function(){
        return conf.botId;
    },
}