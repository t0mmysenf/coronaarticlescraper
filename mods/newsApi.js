const superagent = require("superagent");
const Article = require("../classes/Article");
const service = require("../services/dbconnection");
const confservice = require("../services/confservice");

//API-Key: 4a83a27f95d84479b171e3e1c10a9e4c
//API-URL: http://newsapi.org/v2/top-headlines
const apiUrl = "http://newsapi.org/v2/top-headlines";

//superagent Doku:      https://newsapi.org/docs/get-started
// Superagent Beispiel: https://visionmedia.github.io/superagent/
// Promises:            https://softwareengineering.stackexchange.com/questions/279898/how-do-i-make-a-javascript-promise-return-something-other-than-a-promise

module.exports = {
    //Gibt eine Liste von Artikeln zurück
    request: function(countryCode){
        return new Promise((resolve, reject) => {
            superagent.get(apiUrl)
                  .query("country=" + countryCode)
                  .set("x-api-key", "4a83a27f95d84479b171e3e1c10a9e4c")
                  .set('accept', 'json')
                  .then(res => {
                      resolve(JSON.stringify(res.body));
                  })
                  .catch(err => {
                      console.log(err);
                      reject("");
                  });
        })
    },

    parseArticles: function(requestBody){
        var jsonstring = JSON.parse(requestBody);
        var articleList = jsonstring.articles;
        //console.log(jsonstring.status);
        for(var i in articleList){
            var currentArticle = new Article;
            
            try{
                //Artikel abfuellen
                var src = articleList[i].source.name;
                currentArticle.source = src;
                currentArticle.author = articleList[i].author;
                currentArticle.title = articleList[i].title;
                currentArticle.description = articleList[i].description;
                currentArticle.url = articleList[i].url;
                currentArticle.date = new Date(articleList[i].publishedAt);
                
                var searchterms = confservice.getSearchKeywords();
                for(var o in searchterms){
                    if(currentArticle.title.toUpperCase().includes(searchterms[o].toUpperCase())){
                        service.insertArticle(currentArticle);
                    }
                }
            } catch(e) {
                console.log("Ein Fehler ist beim Abfüllen von Article aufgetreten: " + e);
            }
            

        }
        
    }
}

